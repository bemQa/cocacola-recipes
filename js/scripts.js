$(document).ready(function(){

	function applyFilters(){
		if($('.tags .button.active').length) {
			$('.recipe').hide();
			$('.tags .button.active').each(function(){
				var tag = $(this);
				var category = tag.text();
				$('.recipe').each(function(){
					var elem = $(this);
					elem.find('.category:contains("'+category+'")').parents('.recipe').show();
				});
			});
		}
		else{
			$('.recipe').show();
		}
		if($('.tagline a.coca-color').length) {
			var arr=[];
			$('.tagline a.coca-color').each(function(){
				var tag = $(this).text();
				$('.recipe:visible').each(function(){
					var elem = $(this);
					var good = elem.find('.cooking-content:contains("'+tag+'")').parents('.recipe');
					if(good.length){
						arr.push(good);
					}
				});
			});
			$('.recipe').hide();
			$(arr).each(function(){
				$(this).show();
			});
		}
	}
	
	$('.modal').modal();
	var url = window.location.href;
	if(url.indexOf('#recipe1') > 0){
        $('#recipe1').modal('open');
    }
    if(url.indexOf('#recipe2') > 0){
        $('#recipe2').modal('open');
    }
    if(url.indexOf('#recipe3') > 0){
        $('#recipe3').modal('open');
    }
    if(url.indexOf('#recipe4') > 0){
        $('#recipe4').modal('open');
    }
    if(url.indexOf('#recipe5') > 0){
        $('#recipe5').modal('open');
    }
    if(url.indexOf('#recipe6') > 0){
        $('#recipe6').modal('open');
    }

	if($(".like").hasClass('active')) {
		$('.likes').removeClass('disabled');
	}
	$('.likes').click(function(){
		$('.recipe').show();
		$(this).toggleClass('active');
		$('.search').toggleClass('disabled');
		$('#search').val('');
		$(".recipe").unmark();
		if(!$(".like").hasClass('active') && !$('.likes').hasClass('active')) {
			$('.likes').addClass('disabled');
		}
		$('.recipe').removeClass('hide-search hide-tags hide-category');
		$('.tagline a').removeClass('coca-color');
		$('.tags .button').removeClass('active');
		$('.tags').toggleClass('hide');
		if($(this).hasClass('active')){
			$(".like").each(function(){
		    	if(!$(this).hasClass('active')) {
					$(this).parents('.recipe').hide();
				}
		  	});
		}
	});

	// при клике на тэг сначала показываем все, потом для каждого тэга: прячем элементы у которых нет класса от этого тега
	// перед этим ифка - есть ли активные тэги, если нет, то показываем все
	$('.tags .button').click(function(){
		$(this).toggleClass('active');
		applyFilters();
	});
	// конец
	// аналогичная функция с хэштегами
	$('.tagline a').click(function(){
		$(this).toggleClass('coca-color');
		applyFilters();
	});
	// функция устанавливающая куки, хранящие состояния like'ов
	function cookieFromLike()
	{
	  	var ch = [];
	  	$(".like").each(function(){
	    	var $el = $(this);
	    	if($el.hasClass("active"))
	      	ch.push($el.attr("id"));
	  	});
	  	$.cookie("likeCookie", ch.join(','));
	}
	// функция восстанавливающая состояния like'ов по кукам
	function likeFromCookie()
	{
	  	if($.cookie("likeCookie") == null)
			return;
	  	var chMap = $.cookie("likeCookie").split(',');
		if(chMap && chMap[0]!=""){
            for (var i in chMap) {
                $('#'+chMap[i]).addClass("active");
            }
            $('.likes').removeClass('disabled');
		}

	}
	// функция сбрасывающая куки с значениями like'ов
	function clearCookie()
	{
	  	$.cookie("likeCookie", null);
	} 
	// проверим, были ли установлены ранее кукисы с именем likeCookie.
	// если нет - установим их.
	var likeCookie = $.cookie("likeCookie");
	if(likeCookie == null)
	{
	  	cookieFromLike();
	  	likeCookie = $.cookie("likeCookie");
	}
	else
	  	likeFromCookie();

	$('.like').click(function(){
		$(this).toggleClass('active');
		cookieFromLike();
		if($(".like").hasClass('active')) {
			$('.likes').removeClass('disabled');
		}
		if(!$(".like").hasClass('active')) {
			$('.likes').addClass('disabled');
		}
		if(!$(".like").hasClass('active') && $('.likes').hasClass('active')) {
			$('.likes').removeClass('disabled');
		}
		if(!$(".like").hasClass('active') && !$('.likes').hasClass('active')) {
			$('.likes').addClass('disabled');
		}
	});

	$(function() {

	  	var mark = function() {
	  		applyFilters();
		    // Read the keyword
		    var keyword = $("#search").val();
		    // Remove previous marked elements and mark
		    // the new keyword inside the context
		    $(".recipe").unmark({
		      	done: function() {
		        	$(".recipe:visible").mark(keyword, {
		        		"separateWordSearch": false
		        	});
		      	}
		    });
		    if(keyword.length){
			    $(".recipe:visible").each(function(){
					if(!$(this).find('mark').length) {
			        	$(this).hide();
			    	}
			    });
		    }

	  	};

	  	$("#search").on("input", mark);

	});
});